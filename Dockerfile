# Symfony 1.4
#
# VERSION	1.0

# use the ubuntu base image provided by dotCloud
FROM ubuntu:22.04

# Environment variables
ENV ENVIRONMENT DEV
ENV DEBIAN_FRONTEND=noninteractive

# make sure the package repository is up to date
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get install -y ca-certificates
RUN apt-get install -y unzip
RUN apt-get install -y curl
RUN apt-get install -y inetutils-ping
RUN apt-get install -y telnet
# # install editor tool
RUN apt-get -y install less
RUN apt-get -y install vim
RUN apt-get -y install joe
RUN apt-get -y install nano
RUN apt-get -y install git

RUN add-apt-repository ppa:ondrej/php
RUN apt-get update

# # install php
RUN apt-get -y install php7.4
RUN apt-get -y install php7.4-mbstring
RUN apt-get -y install php7.4-soap
RUN apt-get -y install php7.4-json
RUN apt-get -y install php7.4-xml
RUN apt-get -y install php7.4-curl
RUN apt-get -y install php7.4-gd
RUN apt-get -y install php7.4-fpm
RUN apt-get -y install php7.4-common
RUN apt-get -y install php7.4-cli
RUN apt-get -y install php7.4-stomp

# Install composer
RUN curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php
RUN HASH=`curl -sS https://composer.github.io/installer.sig`
RUN php -r "if (hash_file('SHA384', '/tmp/composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN /etc/init.d/php7.4-fpm start

# customize php7.4
ADD config/php7.4/fpm/php-fpm.conf /etc/php/7.4/fpm/php-fpm.conf
ADD config/php7.4/fpm/php.ini /etc/php/7.4/fpm/php.ini
ADD config/php7.4/fpm/pool.d/www.conf /etc/php/7.4/fpm/pool.d/www.conf
ADD config/php7.4/cli/php.ini /etc/php/7.4/cli/php.ini

RUN /etc/init.d/php7.4-fpm restart

# expose http & ssh port
EXPOSE 9000

# Custom scripts
ADD config/usr/local/bin/core-entrypoint.sh /usr/local/bin/core-entrypoint.sh
RUN chmod 777 /usr/local/bin/core-entrypoint.sh

ENTRYPOINT core-entrypoint.sh
