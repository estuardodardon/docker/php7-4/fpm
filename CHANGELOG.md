# Change Log

All notable changes to this project will be documented in this file.

## [Unreleased] - 2023-02-06

minor improves on core scripts

### New files

- **Changelog.md**: Added changelog file to the project
- **config/php7.4/cli/php.ini**: Added php.ini for cli environment

### Changed files

- **config/usr/local/bin/core-entrypoint.sh**: Change /dev/null to /var/log/php7.4-fpm.log
